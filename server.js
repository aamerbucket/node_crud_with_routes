const express = require('express');
const app = express();
const mongoose = require('mongoose');

app.use(express.json());

mongoose.connect('mongodb://localhost/school')
.then(()=>{console.log('connected to db')})
.catch(()=>{console.error('something went wrong with the db')});

const School = mongoose.model('School', new mongoose.Schema({
    name: String,
    address: String,
    rank: Number
}));

app.post('/', async (req, res)=>{
    let school = new School({
        name: req.body.name,
        address: req.body.address,
        rank: req.body.rank
    });
    school = await school.save();
    res.send(school);
});

const PORT = 3000;
app.listen(PORT, ()=> {console.log(`sever is started on ${PORT}`)});