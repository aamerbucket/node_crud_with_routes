const express = require('express');
const app = express();
const mongoose = require('mongoose');
const router = require('./routes/school');

mongoose.connect('mongodb://localhost/school')
.then(()=>{console.log('connected to db')})
.catch(()=>{console.error('something went wrong with the db')});

app.use(express.json());
app.use('/api/school', router);

const PORT = 3000;
app.listen(PORT, ()=> {console.log(`sever is started on ${PORT}`)});