const mongoose = require('mongoose');

const School = mongoose.model('School', new mongoose.Schema({
    name: String,
    address: String,
    rank: Number
}));

module.exports = School;