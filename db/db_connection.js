const mongoose = require('mongoose');

// create schema and compile its
const School = mongoose.model('School', new mongoose.Schema({
    name: String,
    address: String,
    rank: Number
}));

exports.School = School;

