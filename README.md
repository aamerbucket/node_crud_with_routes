# School - CRUD Operations Node and MongoDB

# Follow the steps
* Create server.js file all code will be inside it.
1. Create server 
    * install express and load it
    * Run and check it with exceptions
1. Create db connection with database
    * install mongoose and load it
    * Create connection
    * Run and check the database the connection in server.js
    * Create and compile the schema
1. Create post method
    * Create new school instance
    * Read restful data from req.body.name etc
    * Save this new instace ` await school.save(); `
    * Make sure you have added the async into the post method
    * Send it back to the client

# Best practices - Routes
1. Create main.js file - it will be used as server file
1. Create model folder and define schema here make sure you export it ` module.exports = School; ` 
1. Create Routes folder
    * Insdie route.js file
    * Load model from model folder
    * Load Express and get router ` const router = express.Router(); `
    * Perform POST, GET, Delete, Put (CRUD Operations)
    * export the router ` module.exports = router; `
1. Inside of main.js
    * Load router
    * Load it to middleware ` app.use('/api/school', router); `
1. Run 
    * nodemon main.js
    * Go to Postman - hit ` http://localhost:3000/api/school `



