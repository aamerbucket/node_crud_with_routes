const express = require('express');
const router = express.Router();
const School = require('../model/school');

router.post('/', async (req, res)=>{
    let school = new School({
        name: req.body.name,
        address: req.body.address,
        rank: req.body.rank
    });
    school = await school.save();
    res.send(school);
});

module.exports = router;